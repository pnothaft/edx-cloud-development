let num = 7
var mystr = "Peter"
mystr += 6
console.log(num)
console.log(mystr)
console.log(num,mystr, 'text')

function printMyInput(user_inmput) {
    console.log("The parameter passed is "+user_inmput)
}

printMyInput(9)
printMyInput("Peter")

let printMyInputES6 = (user_input)=>{
  console.log(user_input)
}

let printMyInputES6_ = user_input => console.log(user_input)

printMyInputES6("Not")
printMyInputES6_(7)

console.log("5==='5'", 5==='5')
document.write(mystr, num)